'use strict'
/** @module write
 * Writes a data point to InfluxDB using the Javascript client library with Node.js.
**/

const { InfluxDB, Point } = require('@influxdata/influxdb-client')
const monitor = require('node-active-window');
const sqlite3 = require('sqlite3').verbose();

/** Environment variables **/
const url = 'https://us-east-1-1.aws.cloud2.influxdata.com'
const token = "AtwhQR6lMQSa8ZyxLLqYpZGbCeVJMMyQDl4j_VsHSA3OCc7ecd0w2WymG8-sXDorX6hYUvsutdP81qHFETQlSw=="
const org = 'tejas.d.talkar@gmail.com'
const bucket = "tejas.d.talkar's Bucket"

/**
 * Instantiate the InfluxDB client
 * with a configuration object.
 **/
const influxDB = new InfluxDB({ url, token })

/**
 * Create a write client from the getWriteApi method.
 * Provide your `org` and `bucket`.
 **/
const writeApi = influxDB.getWriteApi(org, bucket)

/**
 * Apply default tags to all points.
 **/
writeApi.useDefaultTags({ region: 'west' })

/**
 * Create a point and write it to the buffer.
 **/

// connect sqlite db
let db = new sqlite3.Database('./mock.db', sqlite3.OPEN_READWRITE, (err) => {
    if (err) {
        console.error("1",err.message);
    }
    console.log('Connected to the database...');
});

// Create Table in sql
db.serialize(function () {
    db.run("CREATE TABLE IF NOT EXISTS application_data (app TEXT, title TEXT)");
});

monitor.getActiveWindow((err, window) => {
    const app_name = window.app
    const app_title = window.title
    if (!err) {
        // console.log(window.app)
        // console.log(window.title); // { app: 'Code', title: 'test.js - node-active-window - Visual Studio Code' }
    }
    // insert win active data into sqlite
    setInterval(() => {
        db.run("INSERT INTO application_data(app, title) VALUES(?,?)", [app_name, app_title], (err) => {
            if (err) {
                return console.log("2",err.message);
            }
            console.log("inserted into sqlite")
        });
    }, 1000);
});

// Insert into Table  
// const sql= `INSERT into application_data(app,title) VALUES (?, ?)`;


// db.run(
//     sql,
//     ['app_name', 'app_title'],
//     (err) => {
//         if (err) return console.error(err.message)
//         console.log('new row is created..')
//     }
// )

// select data from sqlite 
const sql = `SELECT * FROM application_data`
db.all(sql, [], (err, rows) => {
    if (err) return console.error("3",err.message)
    rows.forEach((rows) => {
        const app_name = rows.app
        const app_title = rows.title
        // console.log(rows.app)
        // console.log(rows.title)
        // insert into influxdb
        const point1 = new Point('aasa_tech')
            .tag('sensor_id', 'google_crome')
            // .floatField('value', 24.0)
            .stringField(app_name, app_title)
        // console.log(` ${point1}`)

        setInterval(() => {
            writeApi.writePoint(point1)
            const sqlDelete = `DELETE FROM application_data`
            db.all(sqlDelete, [], (err) => {
                if (err) return console.error("4",err.message)
                console.log('delete successfully...')
            })
        }, 30000000000000);
        console.log('data insert into influx')


    })
})

// delete sql table data after 30 sec




// db.close((err) => {
//     if (err) return console.error(err.message)
// })




/**
 * Flush pending writes and close writeApi.
 **/
// writeApi.close().then(() => {
//     console.log('WRITE FINISHED')
// })

