const express = require('express')
const app = express()
const port = 3000

const { InfluxDB } = require('@influxdata/influxdb-client')

// You can generate an API token from the "API Tokens Tab" in the UI
const token = "AtwhQR6lMQSa8ZyxLLqYpZGbCeVJMMyQDl4j_VsHSA3OCc7ecd0w2WymG8-sXDorX6hYUvsutdP81qHFETQlSw=="
const org = 'tejas.d.talkar@gmail.com'
const bucket = "tejas.d.talkar's Bucket"

const client = new InfluxDB({ url: 'https://us-east-1-1.aws.cloud2.influxdata.com', token: token })
const { Point } = require('@influxdata/influxdb-client')

app.get('/', (req, res) => {
  const writeApi = client.getWriteApi(org, bucket)
  writeApi.useDefaultTags({ host: 'host1' })

  const point = new Point('meme').floatField('used_percents', 23.43234543)
  writeApi.writePoint(point)

  writeApi
    .close()
    .then(() => {
      console.log('FINISHED')
      res.send('FINISHED')
    })
    .catch(e => {
      console.error(e)
      console.log('Finished ERROR')
      res.send('FINISHED ERROR')
    })
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})

